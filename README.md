Hello Logify!!

# Before everything

Create a .env file based on local.env and change de mongoDB URI with user and password

# Starting the Application without nodemon

\$ yarn start

# Starting the Application with nodemon watching changes

\$ yarn dev

# Starting the Application in Debugging mode

\$ node --inspect app.js `Exposing the debug port publicly is unsafe`
