const express = require("express");
const router = express.Router();
const { isLoggedIn } = require("../../helpers/auth");
const { WalletModel } = require("../../models");

// List all
router.get("/", isLoggedIn, (req, res) => {
  WalletModel.find()
    .sort({ created_at: "desc" })
    .then(wallets => {
      res.render("admin/wallet", {
        wallets: wallets
      });
    })
    .catch(error => {
      req.flash("wallets not found");
      res.redirect("/admin/wallet");
    });
});

module.exports = router;
