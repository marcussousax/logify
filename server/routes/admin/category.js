const express = require("express");
const router = express.Router();
const { isLoggedIn } = require("../../helpers/auth");
const CategoryModel = require("../../models/Category");

// List all
router.get("/", isLoggedIn, (req, res) => {
  CategoryModel.find()
    .sort({ created_at: "desc" })
    .then(categories => {
      res.render("admin/category", {
        categories: categories
      });
    })
    .catch(error => {
      req.flash("failed to retrieve categories");
      res.redirect("/admin/category");
    });
});

// Create category
router.get("/add", isLoggedIn, (req, res) => {
  res.render("admin/category/form");
});

// Edit category
router.get("/edit/:id", isLoggedIn, (req, res) => {
  CategoryModel.findOne({ _id: req.params.id })
    .then(category => {
      res.render("admin/category/edit", { category: category });
    })
    .catch(() => {
      req.flash("error_msg", "category not found");
      res.redirect("/admin/category");
    });
});

// POST METHODS
// Create
router.post("/post", isLoggedIn, (req, res) => {
  // first we should create an empty array for the errors
  let errors = [];

  const { name } = req.body;

  if (!name || typeof name == undefined || name == null) {
    errors.push({
      text: "invalid name"
    });
  }

  if (name.length < 2) {
    errors.push({
      text: "name need to have more than 2 characters"
    });
  }

  if (errors.length > 0) {
    res.render("admin/category/form", { errors: errors });
  } else {
    const category = {
      name: req.body.name
    };

    CategoryModel.findOne({ name: name })
      .then(name => {
        if (name) {
          req.flash("error_msg", "category already exists");
          res.redirect("/admin/category");
        } else {
          new CategoryModel(category)
            .save()
            .then(() => {
              req.flash("success_msg", "category created");
              res.redirect("/admin/category");
            })
            .catch(error => {
              req.flash("error_msg", "failed to save category");
              console.log("failed to save category" + error);
            });
        }
      })
      .catch(error => console.log(error));
  }
});

// Edit
router.post("/edit", isLoggedIn, (req, res) => {
  CategoryModel.findOne({ _id: req.body.id })
    .then(category => {
      category.name = req.body.name;
      category
        .save()
        .then(() => {
          req.flash("success_msg", "category updated with success");
          res.redirect("/admin/category");
        })
        .catch(error => {
          req.flash("error_msg", "failed to update");
          res.redirect("/admin/category");
        });
    })
    .catch(error => {
      req.flash("error_msg", "category not found");
      res.redirect("/admin/category");
    });
});

// Delete
router.post("/delete", isLoggedIn, (req, res) => {
  CategoryModel.deleteOne({ _id: req.body.id })
    .then(() => {
      req.flash("success_msg", "category removed");
      res.redirect("/admin/category");
    })
    .catch(error => {
      req.flash("error_msg", "failed to remove category");
      res.redirect("/admin/category");
    });
});

module.exports = router;
