const express = require("express");
const router = express.Router();
const { isLoggedIn } = require("../../helpers/auth");
const { PostModel } = require("../../models");
const Category = require("../../models/Category");

// List all
router.get("/", isLoggedIn, (req, res) => {
  PostModel.find({ author: req.user._id })
    .populate("category")
    .sort({ created_at: "desc" })
    .then(posts => {
      res.render("admin/post", {
        posts: posts
      });
    })
    .catch(error => {
      req.flash("posts not found");
      res.redirect("/admin/post");
    });
});

// Create post
router.get("/add", isLoggedIn, (req, res) => {
  Category.find()
    .then(categories => {
      res.render("admin/post/form", { categories: categories });
    })
    .catch(error => {
      req.flash("error_msg", "failed to fetch categories");
      res.redirect("/admin/post");
    });
});

// Edit Post
router.get("/edit/:id", isLoggedIn, (req, res) => {
  PostModel.findOne({ _id: req.params.id }) // using req.params because the _id comes via url params
    .then(post => {
      Category.find()
        .then(categories => {
          res.render("admin/post/edit", {
            post: post,
            categories: categories
          });
        })
        .catch(error => {
          req.flash("error_msg", "category not found");
          console.log(error);
        });
    })
    .catch(() => {
      req.flash("error_msg", "post not found");
      res.redirect("/admin/post");
    });
});

// POST methods
router.post("/post", isLoggedIn, (req, res) => {
  // first we should create an empty array for the errors
  let errors = [];

  const { title, category } = req.body;

  if (!title || typeof title == undefined || title == null) {
    errors.push({
      text: "title can't be empty"
    });
  }
  
  if (category == 0) {
    errors.push({
      text: "you need to choose a category"
    });
  }

  if (errors.length > 0) {
    res.render("admin/post/form", { errors: errors });
  } else {
    const post = {
      author: req.user._id,
      title: title,
      category: category
    };

    new PostModel(post)
      .save()
      .then(() => {
        req.flash("success_msg", "post created");
        res.redirect("/admin/post");
      })
      .catch(error => {
        req.flash("error_msg", "failed to create a post");
        console.log("Failed to create a post " + error);
        res.redirect("/admin/post");
      });
  }
});

router.post("/edit", isLoggedIn, (req, res) => {
  PostModel.findOne({ _id: req.body.id })
    .then(post => {
      post.title = req.body.title;
      post.category = req.body.category;
      post
        .save()
        .then(() => {
          req.flash("success_msg", "post updated with success");
          res.redirect("/admin/post");
        })
        .catch(error => {
          req.flash("error_msg", "failed to update the post");
          res.redirect("/admin/post");
        });
    })
    .catch(error => {
      req.flash("error_msg", "post not found");
      res.redirect("/admin/post");
    });
});

// Delete
router.post("/delete", isLoggedIn, (req, res) => {
  PostModel.deleteOne({ _id: req.body.id })
    .then(() => {
      req.flash("success_msg", "post removed");
      res.redirect("/admin/post");
    })
    .catch(error => {
      req.flash("error_msg", "failed to remove the post");
      res.redirect("/admin/post");
    });
});

module.exports = router;
