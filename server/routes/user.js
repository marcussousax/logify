const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const passport = require("passport");
const { UserModel } = require("../models");


router.get("/login", (req, res) => {
  res.render("user/login");
});

router.post("/login", (req, res, next) => {
  passport.authenticate("local", {
    successRedirect: "/",
    failureRedirect: "/user/login",
    failureFlash: true
  })(req, res, next);
});

router.get("/logout", (req, res) => {
  req.logout();
  req.flash("success_msg", "logged out from system");
  res.redirect("/");
});

router.get("/register", (req, res) => {
  req.flash("error_msg", "account creation disabled");
  res.redirect("/");
  // res.render("user/register");
});

router.post("/register", (req, res) => {
  let errors = [];

  const { name, email, password, password2 } = req.body;

  if (!name || typeof name == undefined || name == null) {
    errors.push({ text: "invalid user" });
  }

  if (!email || typeof email == undefined || email == null) {
    errors.push({ text: "invalid email" });
  }

  if (!password || typeof password == undefined || password == null) {
    errors.push({ text: "invalid password" });
  }

  if (password !== password2) {
    errors.push({ text: "password mismatch" });
  }

  if (errors.length > 0) {
    res.render("user/register", { errors: errors });
  } else {
    UserModel.findOne({ email: email })
      .then(user => {
        if (user) {
          req.flash("error_msg", "user exists");
          res.redirect("/user/register");
        } else {
          // A new instance of User model must be stored in a const
          // because we need to encrypt password first, then
          // we must change the the password value with the new hash value.
          const newUser = new UserModel({
            name: name,
            email: email,
            password: password
          });

          bcrypt.genSalt(10, (error, salt) => {
            bcrypt.hash(newUser.password, salt, (error, hash) => {
              if (error) {
                req.flash("error_msg", "failed to create user");
                res.redirect("/user/register");
              }

              newUser.password = hash;
              newUser
                .save()
                .then(() => {
                  req.flash("success_msg", "user created");
                  res.redirect("/");
                })
                .catch(error => {
                  req.flash("error_msg", "failed to create user");
                  res.redirect("/user/register");
                });
            });
          });
        }
      })
      .catch(error => {
        req.flash("error_msg", "failed to search if user already exists");
        console.log("failed to search if user already exists: ", error);
        res.redirect("/user/register");
      });
  }
});

module.exports = router;
