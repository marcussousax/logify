const UserModel = require("./User/User");
const WalletModel = require("./Wallet/Wallet");
const PostModel = require("./Post/Post");

module.exports = {
  UserModel,
  WalletModel,
  PostModel
};
