const mongoose = require("mongoose");
const dbHandler = require("../../config/db-test-handler");
const PostModel = require("./Post");

const currentDate = new Date("2019-05-14T11:01:58.135Z");

const expectedPost = {
  title: "wallet-test",
  created_at: currentDate,
  category: mongoose.Types.ObjectId("5e67e454ee712b6f652739b4"),
  author: mongoose.Types.ObjectId("53cb6b9b4f4ddef1ad47f943")
};

describe("Post Model Test", function() {
  beforeAll(async () => await dbHandler.connect());
  afterEach(async () => await dbHandler.clearDatabase());
  afterAll(async () => await dbHandler.closeDatabase());

  it("should create a post", async function() {
    const validPost = new PostModel(expectedPost);
    const savedPost = await validPost.save();

    expect(savedPost._id).toBeDefined();
    expect(savedPost.title).toBe(expectedPost.title);
    expect(savedPost.created_at).toBe(expectedPost.created_at);
    expect(savedPost.category).toBe(expectedPost.category);
    expect(savedPost.author).toBe(expectedPost.author);
  });
});
