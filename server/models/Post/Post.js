const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PostSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  category: {
    type: Schema.Types.ObjectId,
    ref: "categories", // Model name of Categories
    required: true
  },
  created_at: {
    type: Date,
    default: Date.now()
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: "users"
  }
});

const PostModel = mongoose.model("posts", PostSchema);
module.exports = PostModel;
