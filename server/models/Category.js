const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CategorySchema = new Schema({
  name: {
    type: String,
    required: true
  },
  created_at: {
    type: Date,
    default: Date.now()
  }
});

const CategoryModel = mongoose.model("categories", CategorySchema);
module.exports = CategoryModel;
