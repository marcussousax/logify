const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Using index: true to optimize queries that use these fields.

const UserSchema = new Schema({
  name: {
    type: String,
    required: true,
    lowercase: true
  },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    index: true
  },
  password: {
    type: String,
    required: true
  },
  isAdmin: {
    type: Number,
    default: 0
  },
  created_at: {
    type: Date,
    default: Date.now()
  }
});

const UserModel = mongoose.model("users", UserSchema);
module.exports = UserModel;
