const dbHandler = require("../../config/db-test-handler");

const UserModel = require("./User");

const currentDate = new Date("2019-05-14T11:01:58.135Z");

const userData = {
  name: "user",
  email: "user@teste.com",
  password: "user",
  isAdmin: 0,
  created_at: currentDate
};

describe("User Model Test", () => {
  beforeAll(async () => await dbHandler.connect());
  afterEach(async () => await dbHandler.clearDatabase());
  afterAll(async () => await dbHandler.closeDatabase());

  it("create & save user successfully", async () => {
    const validUser = new UserModel(userData);
    const savedUser = await validUser.save();

    // Object Id should be defined when successfully saved to MongoDB.
    expect(savedUser._id).toBeDefined();
    expect(savedUser.name).toBe(userData.name);
    expect(savedUser.email).toBe(userData.email);
    expect(savedUser.password).toBe(userData.password);
    expect(savedUser.isAdmin).toBe(userData.isAdmin);
    expect(savedUser.created_at).toBe(userData.created_at);
  });

  it("insert user successfully, but the field does not defined in schema should be undefined", async () => {
    const userWithInvalidField = new UserModel(userData);
    const savedUserWithInvalidField = await userWithInvalidField.save();
    expect(savedUserWithInvalidField._id).toBeDefined();
    expect(savedUserWithInvalidField.nickname).toBeUndefined();
  });
});
