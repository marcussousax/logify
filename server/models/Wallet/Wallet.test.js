const mongoose = require("mongoose");
const dbHandler = require("../../config/db-test-handler");
const WalletModel = require("./Wallet");

const currentDate = new Date("2019-05-14T11:01:58.135Z");

const expectedWallet = {
  name: "wallet-test",
  created_at: currentDate,
  author: mongoose.Types.ObjectId("53cb6b9b4f4ddef1ad47f943")
};

describe("Wallet Model Test", function() {
  beforeAll(async () => await dbHandler.connect());
  afterEach(async () => await dbHandler.clearDatabase());
  afterAll(async () => await dbHandler.closeDatabase());

  it("should create a wallet", async function() {
    const validWallet = new WalletModel(expectedWallet);
    const savedWallet = await validWallet.save();

    expect(savedWallet._id).toBeDefined();
    expect(savedWallet.name).toBe(expectedWallet.name);
    expect(savedWallet.created_at).toBe(expectedWallet.created_at);
    expect(savedWallet.author).toBe(expectedWallet.author);
  });
});
