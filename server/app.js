const app = require("./config/server");
const mongoose = require("mongoose");
const db = require("./config/db");
const Handlebars = require("handlebars");
const handlebars = require("express-handlebars");
const HandlebarsIntl = require("handlebars-intl");
const {
  allowInsecurePrototypeAccess
} = require("@handlebars/allow-prototype-access");

// Models
const { PostModel } = require("./models");

// Route Modules
const post = require("./routes/admin/post");
const category = require("./routes/admin/category");
const wallet = require("./routes/admin/wallet");
const user = require("./routes/user");

// STATIC
const HOSTNAME = "127.0.0.1";
const PORT = process.env.PORT || 3000;

// Handlebars Config
app.engine(
  "handlebars",
  handlebars({
    handlebars: allowInsecurePrototypeAccess(Handlebars),
    defaultLayout: "main",
    registerPartial: "partials"
  })
);
HandlebarsIntl.registerWith(Handlebars);
app.set("view engine", "handlebars");

// Mongoose Config
mongoose.set("useCreateIndex", true);
mongoose.Promise = global.Promise;
mongoose
  .connect(db.mongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => console.log("MongoDB connected."))
  .catch(error => {
    console.log("Can't connect to database: ", error);
  });

// Route definitions
app.get("/", async (req, res) => {
  await PostModel.find(req.user && { author: req.user._id })
    .sort({ data: "DESC" })
    .populate("category")
    .then(post => {
      res.render("partials/home", { post: post });
    })
    .catch(error => console.log(error));
});

app.get("/404", async (req, res) => await res.send("page not found"));
app.use("/admin/post", post);
app.use("/admin/wallet", wallet);
app.use("/admin/category", category);
app.use("/user", user);

app.listen(PORT, () => {
  console.log(`Server running at http://${HOSTNAME}:${PORT}/`);
});
