module.exports = {
  isAdmin: (req, res, next) => {
    if (req.isAuthenticated() && req.user.isAdmin == true) {
      return next();
    }

    req.flash("error_msg", "only admin can access this area");
    res.redirect("/");
  },

  isLoggedIn: (req, res, next) => {
    if (req.isAuthenticated()) {
      return next();
    }

    req.flash("error_msg", "only autenthicated users can access this area");
    res.redirect("/");
  }
};
