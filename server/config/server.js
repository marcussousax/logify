const express = require("express");
const session = require("express-session");
const flash = require("connect-flash");
const bodyParser = require("body-parser");
const passport = require("passport");
require("../config/auth")(passport);

const app = express();

// Session config
app.use(
  session({
    secret: "logify",
    resave: true,
    saveUninitialized: true
  })
);

app.use(passport.initialize());
app.use(passport.session());

// Flash needs to be under session middleware
// A flash message (Success or Error) that vanishes itself on reload
app.use(flash());

// Middleware to store global variables
app.use((req, res, next) => {
  res.locals.success_msg = req.flash("success_msg");
  res.locals.error_msg = req.flash("error_msg");
  res.locals.error = req.flash("error");
  res.locals.user = req.user || null;
  next();
});

// Body Parser config
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Static Filtes
app.use(express.static("public"));

module.exports = app;
