const localStrategy = require("passport-local").Strategy;
const bcrypt = require("bcryptjs");

const { UserModel } = require("../models");

module.exports = passport => {
  passport.use(
    new localStrategy(
      { usernameField: "name", passwordField: "password" },
      (name, password, done) => {
        UserModel.findOne({ name: name }).then(user => {
          if (!user) {
            return done(null, false, { message: "user not found" });
          }

          // it compares password from form req with model password
          bcrypt.compare(password, user.password, (error, match) => {
            if (match) {
              return done(null, user);
            } else {
              return done(null, false, { message: "wrong password" });
            }
          });
        });
      }
    )
  );

  passport.serializeUser((user, done) => {
    done(null, user.id);
  });

  passport.deserializeUser((id, done) => {
    UserModel.findById(id, (error, user) => {
      done(error, user);
    });
  });
};
